# Mcom Socket Server

## Introduction

> User Facing Socket Node Server

## Code Samples

```javascript
git clone https://diepmyduong@bitbucket.org/diepmyduong/mcom-socket-server.git
```
```javascript
npm i
node server
```

## Installation

> ```
docker pull jerryip/socket-server
docker run --rm -p 8000:8000 --name socket-server jerryip/socket-server
```