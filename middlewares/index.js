module.exports = function(scServer) {
    const handshake = require('./handshake');
    const emit = require('./emit');
    const authenticate = require('./authenticate');
    const subscribe = require('./subscribe');
    const publishIn = require('./publish-in')
    scServer.addMiddleware(scServer.MIDDLEWARE_HANDSHAKE_WS, handshake.handler.bind(handshake));
    scServer.addMiddleware(scServer.MIDDLEWARE_AUTHENTICATE, authenticate.handler.bind(authenticate));
    scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE, subscribe.handler.bind(subscribe));
    scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN, publishIn.handler.bind(publishIn));
    scServer.addMiddleware(scServer.MIDDLEWARE_EMIT, emit.handler.bind(emit));
}