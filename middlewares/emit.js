const EVENTS = require('../socket_events/events');

class EmitMiddleware {
    constructor() {}
    
    handler(req, next) {
        const { socket, event, authTokenExpiredError, data } = req;
        if (authTokenExpiredError) {
            next(authTokenExpiredError);
            return;
        }
        switch (event) {
            case EVENTS.ADMIN_LOGIN: 
            case EVENTS.LOGIN:
                next(); break;
            default: this.requireAdminAuth(req, next);
        }
    }

    requireAdminAuth(req, next) {
        const { socket } = req;
        const authToken = socket.authToken;
        if (authToken && authToken.isAdmin) next();
        else next('Permission Deny');
    }
}

module.exports = new EmitMiddleware();
