class SubscribeMiddleware {
    constructor() {}
    
    handler(req, next) {
        
        const { socket, channel, authTokenExpiredError, waitForAuth, data } = req;
        const { authToken } = socket;
        console.log('on subscirbe', authToken);
        if(authTokenExpiredError) next(authTokenExpiredError);
        else if(authToken && (authToken.subs.indexOf(channel) != -1 || authToken.isAdmin)) next(); 
        else  next('Permission Deny');
    }
}

module.exports = new SubscribeMiddleware();
