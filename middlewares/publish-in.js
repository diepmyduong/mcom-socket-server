class PublishInMiddleware {
    constructor() {}
    
    handler(req, next) {
        const { socket, channel, authTokenExpiredError, data } = req;
        const { authToken } = socket;
        if(authTokenExpiredError) next(authTokenExpiredError);
        else if(authToken && authToken.isAdmin) next();
        else next('Permission Deny');
    }
}

module.exports = new PublishInMiddleware();
