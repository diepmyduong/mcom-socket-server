require('dotenv').config();

module.exports = {
    secret: process.env.SECRET || 'my-socket-server-secret'
}