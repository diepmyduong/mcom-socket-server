var util = require('./util');
var config = require('../config');
var moment = require('moment');
class RegisConnectionEvent {
    constructor(socket) {
        this.socket = socket;
    }
    handler(data, res) {
        const connectionToken = util.generateToken(data, 'connection', {
            exp: moment().add(1, 'days'),
            secret: config.secret
        })
        res(null, connectionToken);
    }
}

module.exports = (socket) => {
    return new RegisConnectionEvent(socket);
}