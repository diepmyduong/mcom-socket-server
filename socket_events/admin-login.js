var config = require('../config');
class AdminLoginEvent {
    constructor(socket) {
        this.socket = socket;
    }
    handler(data, res) {
        if(data.secret && data.secret == config.secret) {
            this.socket.setAuthToken({ isAdmin: true });
            res(); 
        } else {
            res("Permission Deny");
        }
        
    }
}

module.exports = (socket) => {
    return new AdminLoginEvent(socket);
}