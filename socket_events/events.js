module.exports = {
    DISCONNECT: 'disconnect',
    ADMIN_LOGIN: 'adminLogin',
    REGIS_CONNECTION: 'regisConnection',
    LOGIN: 'login',
    REGIS_CHANNEL: 'regisChannel',
    REQUEST_CHANNEL: 'requestChannel'
}