var config = require('../config');
var util = require('./util');
class ClientLoginEvent {
    constructor(socket) { this.socket = socket; }
    handler(token, res) {
        try {
            const tokenInfo = util.decodeToken(token, { secret: config.secret })
            if (tokenInfo) {
                this.socket.setAuthToken(tokenInfo.payload);
                res(null, tokenInfo.payload);
            } else {
                res('Bad Token');
            }
        } catch (err) {
            res(err.message);
        }
        
    }
}

module.exports = (socket) => {
    return new ClientLoginEvent(socket);
}