const moment = require('moment')
const jwt = require('jwt-simple')
module.exports = {
    generateToken: function (payload, role, option = {
        exp: moment().add(5, "minutes")
    }) {
        const secret = option.secret
        return jwt.encode({
            payload: payload,
            role: role,
            exp: option.exp
        }, secret);
    },
    decodeToken: function (token, option) {
        let result = undefined
        try {
            const secret = option.secret
            result = jwt.decode(token, secret);
        } catch (err) {
            throw new Error('Bad token')
        }
        if (result) {
            if ((new Date(result.exp)).getTime() <= Date.now()) {
                throw new Error('Token Expired')
            }
            return result;
        } else {
            throw new Error('Bad token')
        }
    }
}