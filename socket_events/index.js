const EVENTS = require('./events');


module.exports = (socket) => {

    const adminLogin = new require('./admin-login')(socket);
    const regisConnection = new require('./regis-connection')(socket);
    const clientLogin = new require('./client-login')(socket);

    socket.on(EVENTS.ADMIN_LOGIN, adminLogin.handler.bind(adminLogin));
    socket.on(EVENTS.REGIS_CONNECTION, regisConnection.handler.bind(regisConnection));
    socket.on(EVENTS.LOGIN, clientLogin.handler.bind(clientLogin));
}